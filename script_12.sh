#!/bin/bash

for i in 1 2 3 4
do
    echo $i
done

for i in {1..20..2}
do
    echo $i
done

for (( i=1; i<19; i++ ))
do
    if [ $i -eq 2 ]
    then
        continue
    fi
    if [ $i -gt 5 ]
    then
        break
    fi
    echo $i
done