#!/bin/bash

car=$1

case $car in
    "BMW" )
        echo "it's BMW" ;;
    "MERCEDES" )
        echo "it's Mercedes" ;;
    * )
        echo "Unknown Model" ;;
esac