#!/bin/bash

age=10

if [ $age -gt 18 ] && [ $age -lt 40 ]
then
    echo "Age is correct"
else
    echo "Age is not correct"
fi

if [ $age -gt 18 && $age -lt 40 ]
then
    echo "Age is correct"
else
    echo "Age is not correct"
fi

if [ $age -gt 18 -a $age -lt 40 ]
then
    echo "Age is correct"
else
    echo "Age is not correct"
fi

if [ $age -gt 18 -o $age -lt 40 ]
then
    echo "age is either greater than 18 or less than 40"
fi