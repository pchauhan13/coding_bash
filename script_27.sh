#!/bin/bash

echo 31+22
echo $(( 31 + 22 ))
echo $(( 31 - 22 ))
echo $(( 31 * 22 ))
echo $(( 31 / 22 ))
echo $(( 31 % 22 ))

n1=31
n2=22

echo $( expr $n1 + $n2 )
echo $( expr $n1 - $n2 )
echo $( expr $n1 \* $n2 )
echo $( expr $n1 / $n2 )
echo $( expr $n1 % $n2 )